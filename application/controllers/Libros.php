<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Libros extends CI_Controller {
	public function __construct()
		 {
						 parent::__construct();
						 $this->load->model('LibrosModel');
						 $this->load->helper('url_helper');
						 $this->load->helper('form');
	 				 	 $this->load->library('form_validation');
		 }

	public function index()
	{
		$datos = $this->LibrosModel->get_libros();
    $this->load->view('layout/header');
		$this->load->view('Libros/index', compact('datos'));
    $this->load->view('layout/footer');
	}
	public function create()
	{

    $this->form_validation->set_rules('titulo', 'Titulo', 'required');
    $this->form_validation->set_rules('descripcion', 'Descripcion', 'required');
		$this->form_validation->set_rules('precio', 'Precio', 'required');

    if ($this->form_validation->run() === FALSE)
    {
			$this->load->view('layout/header');
			$this->load->view('Libros/create');
			$this->load->view('layout/footer');
    }
    else
    {
			 $data = array(
                    'titulo' => $this->input->post('titulo'),
										'decripcion' => $this->input->post('descripcion'),
                    'precio' => $this->input->post('precio')
                );
        $this->LibrosModel->set_libros($data);
				$datos = $this->LibrosModel->get_libros();
		    $this->load->view('layout/header');
				$this->load->view('Libros/index', compact('datos'));
		    $this->load->view('layout/footer');
    }
	}
	public function edit()
	{
		$id = $this->input->post('id');
		$datos = $this->LibrosModel->get_libro($id);
		$this->load->view('layout/header');
		$this->load->view('Libros/edit', compact('datos'));
		$this->load->view('layout/footer');
	}
	public function update()
	{
			 $data = array(
										'titulo' => $this->input->post('titulo'),
										'decripcion' => $this->input->post('descripcion'),
										'precio' => $this->input->post('precio')
								);
				$id = $this->input->post('id');

				$this->LibrosModel->update($data,$id);
				$datos = $this->LibrosModel->get_libros();

				$this->load->view('layout/header');
				$this->load->view('Libros/index', compact('datos'));
				$this->load->view('layout/footer');

	}
	public function delete()
	{
		$id = $this->input->post('id');
		$this->LibrosModel->delete($id);
		$datos = $this->LibrosModel->get_libros();

		$this->load->view('layout/header');
		$this->load->view('Libros/index', compact('datos'));
		$this->load->view('layout/footer');
	}
}
