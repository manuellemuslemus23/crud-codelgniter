<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class LibrosModel extends CI_Model {

        public function __construct()
        {
          $this->load->database();
        }
        public function get_libros()
        {
          $query = $this->db->get('libros');
          return $query->result_array();
        }
        public function set_libros($data)
        {
          return $this->db->insert('libros', $data);
        }
        public function get_libro($id)
        {
          $query = $this->db->get_where('libros', array('id' => $id));
          return $query->result_array();

        }
        public function update($data, $id)
        {
          return $this->db->update('libros', $data, array('id' => $id));
        }
        public function delete($id)
        {
          return $query = $this->db->delete('libros', array('id' => $id));
        }
}
?>
